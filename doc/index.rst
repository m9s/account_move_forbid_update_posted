account_move_forbid_update_posted Module
========================================

This module disable
 * Financial Management > Configuration > Journals > Journals
   * Option allow cancelling moves
 
 * Financial Management > Entries > Account Moves
   * Button Draft on validated moves

This module raise immediatly a modify_posted_move error when 
method self.draft(cursor, user, ids, context) is used.

With this restrictions it is not possible to change a move in Tryton which is 
once validated.
   

