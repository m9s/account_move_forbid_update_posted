#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Move Forbid Update Posted',
    'name_de_DE': 'Buchhaltung Buchung Fixierte Festschreibung',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Disable the option for reset a posted move back into state draft.
    ''',
    'description_de_DE': '''
    - Deaktiviert die Möglichkeit, eine einmal festgeschriebene Buchung in
    den Entwurfsstatus zurückzusetzen.
    ''',
    'depends': [
        'account',
    ],
    'xml': [
        'journal.xml',
        'move.xml',
    ],
    'translation': [
    ],
}
