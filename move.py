# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Move(ModelSQL, ModelView):
    _name = 'account.move'

    def draft(self, ids):
        self.raise_user_error(cursor, 'modify_posted_move',
                context=context)

Move()
